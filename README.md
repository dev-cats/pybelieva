# Pybelieva

[![discord][discord-badge]][discord] [![api][api-badge]][api]
![logo][logo]

Pybelieva is a library providing a Python interface
to the [UnbelievaBoat API][api]. To
use it, you need to get a token from the official documentation page.

## Dependencies

- Python
- aiohttp
- discord.py (optional)

## Roadmap

- Documentation
- Rate limit handling

[logo]: https://i.imgur.com/RLRDeQw.png
[discord-badge]: https://img.shields.io/discord/592830069806989339?color=4e5d94&label=Discord&logo=discord
[discord]: https://discord.gg/jK8thg37mC
[api]: https://unb.pizza/api/docs
[api-badge]: https://img.shields.io/badge/api-v1-ff266a
